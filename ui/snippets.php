<div id="wp-small-snippet">
    <div id="snippets-list">
        <h1>Snippet list</h1>
        <ul>
        <?php
            $snippets = get_posts(array(
                    'post_type' => 'wp_small_snippet',
                    'post_per_page' => -1,
                    'post_status' => 'publish'
            ));


            foreach ($snippets as $snippet)
            {
                ?>
                <li><a href="<?php echo admin_url('edit.php?post_type=wp_small_post&page=wp-small-post-snippets&ID=') . $snippet->ID; ?>"><?php echo $snippet->post_title ?></a></li>


            <?php
            }

        ?>

        </ul>


        <a class="button button-primary" href="<?php echo admin_url('edit.php?post_type=wp_small_post&page=wp-small-post-snippets') ?>">Create new snippet</a>
    </div>


    <div id="create-snippets">
        <h1>Create your snippet</h1>

        <?php $current_snippet = (isset($_GET['ID']) ? get_post($_GET['ID']) : null); ?>

        <form action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post">
            <label for="">Title</label>
            <input value="<?php echo ($current_snippet != null ? $current_snippet->post_title : ''); ?>" type="text" id="snippet-title" name="title">
            <label for="">Content</label>
            <textarea  name="content" id="snippet-content" cols="30" rows="20"><?php echo ($current_snippet != null ? $current_snippet->post_content : ''); ?></textarea>

            <input type="hidden" name="ID" value="<?php echo (isset($_GET['ID']) ? $_GET['ID'] : '0' ); ?>">
            <input type="hidden" name="URL" value="<?php echo get_current_screen()->base; ?>">
            <?php wp_nonce_field('wp_small_snippet_save', 'wp_small_snippet_nonce') ?>
            <input type="hidden" name="action" value="wp_small_snippet_save">
            <input type="submit" class="button button-primary" value="Save">
        </form>

    </div>





</div>