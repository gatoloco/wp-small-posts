<?php
/**
 * Created by PhpStorm.
 * User: myn
 * Date: 2/5/19
 * Time: 4:39 PM
 */

add_action('admin_post_wp_small_snippet_save', 'handle_post_wp_small');

function handle_post_wp_small()
{
    if (wp_verify_nonce($_POST['wp_small_snippet_nonce'], 'wp_small_snippet_save'))
    {
        $id = wp_insert_post(array(
            'ID' => $_POST['ID'],
            'post_title' => $_POST['title'],
            'post_content' => $_POST['content'],
            'post_type' => 'wp_small_snippet',
            'post_status' => 'publish'
        ));


        if ($_POST['ID'] == 0)
            wp_redirect($_POST['_wp_http_referer'] . '&ID='. $id);
        else
            wp_redirect($_POST['_wp_http_referer']);





    }
}