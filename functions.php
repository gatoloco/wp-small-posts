<?php
/**
 * Created by PhpStorm.
 * User: myn
 * Date: 9/27/18
 * Time: 11:11 AM
 */
include_once 'handle-snippet.php';

add_action('admin_enqueue_scripts', 'wp_small_post_backend_styles');

function wp_small_post_backend_styles()
{
    wp_register_style('wp-small-post-snippets-backend-styles', plugins_url('css/backend.css', __FILE__));
    wp_enqueue_style('wp-small-post-snippets-backend-styles');
}


//register post type
function wp_small_post_register_post_type()
{


$small_post_labels = array(
    'name' => 'Small Posts',
    'singular_name' => 'Small Post',
    'add_new' => 'Create small post',
    'add_new_item' => 'Create small post',
);
$args = array(
    'labels'             => $small_post_labels,
    'public'             => false,
    'publicly_queryable' => false,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => null,
    'supports'           => array( 'title', 'editor'),
);

register_post_type( 'wp_small_post', $args );



    $snippet_post_labels = array(
        'name' => 'Snippets',
        'singular_name' => 'Snippet',
        'add_new' => 'Create snippet',
        'add_new_item' => 'Create snippet',
    );
    $args = array(
        'labels'             => $snippet_post_labels,
        'public'             => false,
        'publicly_queryable' => false,
        'show_ui'            => false,
        'show_in_menu'       => false,
        'query_var'          => true,
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor'),
    );

    register_post_type( 'wp_small_snippet', $args );


}



add_action('init', 'wp_small_post_register_post_type');

add_action('admin_menu', 'wp_small_post_add_snippets_menu');

function wp_small_post_add_snippets_menu()
{
    add_submenu_page(
        'edit.php?post_type=wp_small_post',
        __( 'Create code snippets', 'wp-small-post' ),
        __( 'Create code snippets', 'wp-small-post' ),
        'manage_options',
        'wp-small-post-snippets',
        'wp_small_post_snippets_ui'
    );
}


function wp_small_post_snippets_ui()
{
    include_once 'ui/snippets.php';
}